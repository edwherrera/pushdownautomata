//
//  PDAutomaton.cpp
//  PushdownAutomata
//
//  Created by Edwin Herrera on 09/03/16.
//  Copyright © 2016 Doninelli. All rights reserved.
//

#include "PDAutomaton.hpp"

PDAutomaton::PDAutomaton() {
    this->_initial = nullptr;
    this->_states = new vector<PDState *>();
}

PDAutomaton::PDAutomaton(vector<CFGVariable> variables) : PDAutomaton(){
    
    //Crear los 3 estados base
    addState("q0");
    addState("q1");
    addState("q2", true);
    
    //crear las transiciones constantes
    addTransition("q0", "q1", "", "Z", variables[0].var + "Z");
    addTransition("q1", "q2", "", "Z", "Z");
    
    //consumir toda la gramatica
    for (auto var : variables) {
        for (auto thing : var.things) {
            if(thing != "") {
                addTransition("q1", "q1", "", var.var, thing);
            }
        }
    }
    
    for (auto var : variables) {
        for (auto thing : var.things) {
            if(thing != "") {
                for (auto c : thing) {
                    if (!isVarName(string(1,c), variables)) {
                        addTransition("q1", "q1", string(1,c), string(1,c), "");
                    }
                }
            }
        }
    }
}

bool PDAutomaton::isVarName(string value, vector<CFGVariable>& vars) {
    for (auto var : vars) {
        if (var.var == value) {
            return true;
        }
    }
    return false;
}

PDAutomaton::~PDAutomaton() {
    for (auto state : *_states) {
        delete state;
    }
    delete _states;
}

bool PDAutomaton::checkInput(string input, vector<string> language) {
    for (auto c : input) {
        string value = string(1,c);
        if (find(language.begin(), language.end(), value) == language.end()) {
            return false;
        }
    }
    return true;
}

bool PDAutomaton::evaluateString(string input) {
    bool valid = true;
    string _start = "Z";
    auto language = getLanguage();
    auto _stack = stack<string>();
    _stack.push(_start);
    PDState * current = _initial;
    
    if (!checkInput(input, language)) {
        valid = false;
    }
    
    for (int i = 0; i < input.length() && valid; i++) {
        string read = string(1, input[i]);
        string pop = _stack.top();
        _stack.pop();
        PDTransition * transition = current->getNext(read, pop);
        if (transition == nullptr) {
            _stack.push(pop);
            valid = false;
        }
        else {
            string push = transition->push;
            if (!push.empty()) {
                for(int h = 0; h < push.length(); h++) {
                    _stack.push(push.substr(push.length()-1-h, push.length()-h));
                }
            }
            current = transition->next;
        }
    }
    
    vector<string> visited = {};
    while (current && !current->isFinal && find(visited.begin(), visited.end(), current->name) == visited.end()) {
        visited.push_back(current->name);
        auto pop = _stack.top();
        _stack.pop();
        auto next = current->getNext("", pop);
        if (next) {
            string push = next->push;
            if (!push.empty()) {
                for(int h = 0; h < push.length(); h++) {
                    _stack.push(push.substr(push.length()-1-h, push.length()-h));
                }
            }
            current = next->next;
        }
        else {
            current = nullptr;
        }
    }
    
    if (!current || !current->isFinal) {
        valid = false;
    }
    
    return valid;
}

PDState * PDAutomaton::addState(string name) {
    return addState(name, false);
}

PDState * PDAutomaton::addState(string name, bool isFinal) {
    PDState * state = getState(name);
    if (state != nullptr) { return nullptr; }
    state = new PDState(name, isFinal);
    _states->push_back(state);
    if(_initial == nullptr) {
        _initial = state;
    }
    return state;
}

PDState * PDAutomaton::getState(string name) {
    for (auto state : *_states) {
        if (state->name == name) {
            return state;
        }
    }
    return nullptr;
}

vector<string> PDAutomaton::getLanguage() {
    vector<string>  language = {};
    for (auto state : *_states) {
        for (auto transition : *(state->transitions)) {
            if (find(language.begin(), language.end(), transition->read) == language.end()) {
                language.push_back(transition->read);
            }
        }
    }
    return language;
}

PDTransition * PDAutomaton::addTransition(string fromStateName, string toStateName, string read, string pop, string push) {
    auto fromState = getState(fromStateName);
    auto toState = getState(toStateName);
    if (fromState == nullptr || toState == nullptr) { return nullptr; }
    auto newTransition = fromState->addTransition(read, pop, push, toState);
    return newTransition;
}

PDState * PDAutomaton::setFinal(string stateName, bool isFinal) {
    auto found = getState(stateName);
    if (found != nullptr) {
        found->setFinal(isFinal);
    }
    return found;
}

PDState * PDAutomaton::setInitial(string stateName) {
    auto found = getState(stateName);
    if (found != nullptr) {
        _initial = found;
    }
    return found;
}

PDState * PDAutomaton::deleteState(string stateName) {
    for (unsigned int i = 0; i < _states->size(); i++) {
        auto stateIter = _states->at(i);
        if (stateIter->name == stateName) {
            _states->erase(_states->begin()+i);
            clearTransitionsToDeletedState(stateName);
            return stateIter;
        }
    }
    return nullptr;
}

void PDAutomaton::clearTransitionsToDeletedState(string stateName) {
    for (auto state : *_states) {
        for (int i = 0; i < state->transitions->size(); i++) {
            auto transitionIt = state->transitions->at(i);
            if(transitionIt->next->name == stateName) {
                state->transitions->erase(state->transitions->begin()+i);
                i--;
                delete transitionIt;
            }
        }
    }
}

