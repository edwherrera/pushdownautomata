//
//  PDState.cpp
//  PushdownAutomata
//
//  Created by Edwin Herrera on 09/03/16.
//  Copyright © 2016 Doninelli. All rights reserved.
//

#include "PDState.hpp"
#include "PDTransition.hpp"

PDState::PDState(string name) : PDState(name,false){}

PDState::PDState(string name, bool isFinal) {
    this->name = name;
    this->isFinal = isFinal;
    this->transitions = new vector<PDTransition *>();
}

PDState::~PDState() {
    for (auto t : *transitions) {
        delete t;
    }
    delete transitions;
}

PDTransition * PDState::addTransition(string read, string pop, string push, PDState * next) {
    PDTransition * newTransition = new PDTransition(read, pop, push, next);
    transitions->push_back(newTransition);
    return newTransition;
}

PDTransition * PDState::getNext(string read, string pop) {
    for (auto transition : *transitions) {
        if (transition->read == read && transition->pop == pop) {
            return transition;
        }
    }
    return nullptr;
}

void PDState::setFinal() {
    setFinal(true);
}

void PDState::setFinal(bool isFinal) {
    this->isFinal = isFinal;
}