//
//  PDState.hpp
//  PushdownAutomata
//
//  Created by Edwin Herrera on 09/03/16.
//  Copyright © 2016 Doninelli. All rights reserved.
//

#ifndef PDState_hpp
#define PDState_hpp

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

class PDTransition;
class PDState {
    
public:
    string name;
    bool isFinal;
    vector<PDTransition *> * transitions;
    
    PDState(string);
    PDState(string,bool);
    ~PDState();
    
    PDTransition * addTransition(string read, string pop, string push, PDState* next);
    PDTransition * getNext(string read, string pop);
    void setFinal();
    void setFinal(bool isFinal);
};

#endif /* PDState_hpp */
