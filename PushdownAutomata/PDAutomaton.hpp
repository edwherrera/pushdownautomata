//
//  PDAutomaton.hpp
//  PushdownAutomata
//
//  Created by Edwin Herrera on 09/03/16.
//  Copyright © 2016 Doninelli. All rights reserved.
//

#ifndef PDAutomaton_hpp
#define PDAutomaton_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <stack>

#include "PDState.hpp"
#include "PDTransition.hpp"

using namespace std;

typedef struct CFGVariable {
    string var;
    vector<string> things;
} CFGVariable;


class PDAutomaton {
public:
    PDAutomaton();
    PDAutomaton(vector<CFGVariable> variables);
    ~PDAutomaton();
    
    bool evaluateString(string input);
    PDState * addState(string name);
    PDState * addState(string name, bool isFinal);
    PDState * getState(string name);
    PDTransition * addTransition(string from, string to, string read, string pop, string push);
    PDState * setFinal(string stateName, bool isFinal);
    PDState * setInitial(string stateName);
    PDState * deleteState(string stateName);
    
private:
    PDState * _initial;
    vector<PDState *> * _states;
    
    bool checkInput(string input, vector<string> language);
    vector<string> getLanguage();
    void clearTransitionsToDeletedState(string stateName);
    bool isVarName(string value, vector<CFGVariable>& vars);
};

#endif /* PDAutomaton_hpp */
