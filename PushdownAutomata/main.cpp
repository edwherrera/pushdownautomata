#include <iostream>
#include <assert.h>

#include "PDAutomaton.hpp"

using namespace std;

void printAndAssert(string input, bool assertCheck, PDAutomaton * automaton);

void example();

int main(int argc, const char * argv[]) {

    CFGVariable var;
    var.var = "E";
    var.things = {"E+T", "E-T", "T"};
    
    CFGVariable var2;
    var2.var = "T";
    var2.things = {"T*P", "P"};
    
    CFGVariable var3;
    var3.var = "P";
    var3.things = {"P^P", "F"};
    
    CFGVariable var4;
    var4.var = "F";
    var4.things = {"n", "(E)"};
    
    vector<CFGVariable> vars = {var, var2, var3, var4};
    
    PDAutomaton * pda = new PDAutomaton(vars);
    
    return 0;
}

void printAndAssert(string input, bool assertCheck, PDAutomaton * automaton) {
    bool result = automaton->evaluateString(input);
    cout << "Result: " << result << endl;
    assert(result == assertCheck);
}

void example() {
    PDAutomaton * automaton = new PDAutomaton();
    
    automaton->addState("q0");
    automaton->addState("q1", false);
    automaton->addState("q2", false);
    automaton->addState("qf", true);
    
    automaton->addTransition("q0", "q0", "a","Z", "aZ");
    automaton->addTransition("q0", "q0", "a","a", "aa");
    automaton->addTransition("q0", "qf", "", "Z", "Z");
    automaton->addTransition("q0", "q1", "b", "a", "a");
    automaton->addTransition("q1", "q2", "b","a", "");
    automaton->addTransition("q2", "q1", "b","a", "a");
    automaton->addTransition("q2", "qf", "","Z", "Z");
    
    printAndAssert("aabbb", true, automaton);
}