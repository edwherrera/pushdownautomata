//
//  PDTransition.hpp
//  PushdownAutomata
//
//  Created by Edwin Herrera on 09/03/16.
//  Copyright © 2016 Doninelli. All rights reserved.
//

#ifndef PDTransition_hpp
#define PDTransition_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

class PDState;
class PDTransition {
public:
    string read;
    string pop;
    string push;
    PDState * next;
    
    PDTransition(string read, string pop, string push, PDState* next);
    ~PDTransition();
};

#endif /* PDTransition_hpp */
