//
//  PDTransition.cpp
//  PushdownAutomata
//
//  Created by Edwin Herrera on 09/03/16.
//  Copyright © 2016 Doninelli. All rights reserved.
//

#include "PDTransition.hpp"
#include "PDState.hpp"

PDTransition::PDTransition(string read, string pop,string push, PDState * next) {
    this->read = read;
    this->pop = pop;
    this->push = push;
    this->next = next;
}

PDTransition::~PDTransition() {
}